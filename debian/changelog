python-stem (1.8.0-3) unstable; urgency=medium

  * Make build reproducible (Closes: #963533)
    thanks to Chris Lamb

 -- Federico Ceratto <federico@debian.org>  Fri, 25 Dec 2020 20:47:27 +0000

python-stem (1.8.0-2) unstable; urgency=high

  * Add Testsuite and Rules-Requires-Root

 -- Federico Ceratto <federico@debian.org>  Thu, 06 Aug 2020 15:49:12 +0100

python-stem (1.8.0-1) unstable; urgency=high

  [ Ulises Vitulli ]
  * New upstream release.
  * Added Federico Ceratto to Uploaders.
  * Updated debian/copyright.
  * Removed py2 support.
  * Updated Standard-version to 4.5.0.2 (no changes needed).

  [ Federico Ceratto ]
  * Bump debhelper from old 12 to 13.
  * Set field Upstream-Name in debian/copyright.

 -- Ulises Vitulli <dererk@debian.org>  Fri, 24 Apr 2020 23:02:39 -0300

python-stem (1.7.1-1) unstable; urgency=medium

  * New upstream release.
    - See https://trac.torproject.org/projects/tor/ticket/28731#comment:18

 -- Ulises Vitulli <dererk@debian.org>  Wed, 26 Dec 2018 20:41:51 -0300

python-stem (1.7.0-1) unstable; urgency=medium

  * New upstream release.
    - Removed local patches since they have been merged upstream or coming
    from it.

 -- Ulises Vitulli <dererk@debian.org>  Tue, 09 Oct 2018 10:53:51 -0300

python-stem (1.6.0-4) unstable; urgency=medium

  * Fix missing Depends: on python3-distutils for py3 pkg. Thanks anbe@!
    (Closes: #905229).
  * ACKed NMU from irl@.
  * Updated Standard-version to 4.2.0.0 (no changes needed).
  * Fix duplicated long pkg description.
  * Enhance local patch description.
  * Added watch file.
  * Added gpg upstream verification.

 -- Ulises Vitulli <dererk@debian.org>  Thu, 02 Aug 2018 10:10:11 -0300

python-stem (1.6.0-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fixes for PyPy compatibility issues (Closes: #904582).

 -- Iain R. Learmonth <irl@debian.org>  Mon, 30 Jul 2018 17:10:46 +0100

python-stem (1.6.0-3) unstable; urgency=medium

  * Prepare package for pypy. Thanks IainRLearmonth! (Closes: #904454).
  * Install some stem usage examples.
  * Update Vcs-* control fields for move to salsa.debian.org.
  * Updated Standard-version to 4.1.5.0 (no changes needed).
  * Updated debhelper compat to 11.
  * Cosmetic update copyrights file.

 -- Ulises Vitulli <dererk@debian.org>  Tue, 24 Jul 2018 11:18:23 -0300

python-stem (1.6.0-2) unstable; urgency=high

  * Patch for py3.7 compat issues on reserved words (Closes: #903860).

 -- Ulises Vitulli <dererk@debian.org>  Mon, 23 Jul 2018 10:25:51 -0300

python-stem (1.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Fix versioned py3.x dependency, thanks MatthiasKlose! (Closes: #880960).
  * Updated Standard-version to 4.1.1.1 (no changes needed).
  * Updated upstream changelog note on large file.
  * Update Vcs-Browser field to https one.

 -- Ulises Vitulli <dererk@debian.org>  Tue, 07 Nov 2017 09:44:55 -0300

python-stem (1.5.3-1) unstable; urgency=medium

  * New upstream release.

 -- Ulises Vitulli <dererk@debian.org>  Tue, 06 Dec 2016 10:51:39 -0300

python-stem (1.5.2-1) unstable; urgency=medium

  * New upstream release.
  * Updated Standard-version to 3.9.8 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Fri, 25 Nov 2016 20:26:18 -0300

python-stem (1.4.1b-3) unstable; urgency=high

  * Use alternatives approach for picking up proper tor-prompt at py2 and py3,
   Thanks DonnchaO'Cearbhaill! (Closes: #812459).
  * Updated Standard-version to 3.9.7 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Sat, 05 Mar 2016 21:23:48 -0300

python-stem (1.4.1b-2) unstable; urgency=medium

  * Rollback #767555 since it kinda loses functionality as it is.
   Thanks lunar@! (Closes: #811373).

 -- Ulises Vitulli <dererk@debian.org>  Mon, 18 Jan 2016 10:55:05 -0300

python-stem (1.4.1b-1) unstable; urgency=medium

  * New upstream release.
  * Updated pkt description.

 -- Ulises Vitulli <dererk@debian.org>  Thu, 02 Jul 2015 19:32:03 -0300

python-stem (1.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Move tor-prompt to examples to avoid provides conflicts (Closes: #767555).
  * Replace upstream changelog from git log to docs/change_log.rst.
  * Updated Standard-version to 3.9.6 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Sun, 04 Jan 2015 12:25:57 -0300

python-stem (1.2.2~de7aca3-1) unstable; urgency=medium

  * New upstream release 1.2.2 (keep reading).
  * Cherry picked upstream/de7aca3 for fixing py incompat.
  * Updated Standard-version to 3.9.5 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Wed, 02 Jul 2014 13:45:52 -0300

python-stem (1.1.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Update Homepage field.
  * debian/copyright: Update Source field.

 -- Ulises Vitulli <dererk@debian.org>  Mon, 21 Oct 2013 13:00:01 -0300

python-stem (1.0.1-3) unstable; urgency=low

  [ Scott Kitterman ]
  * Move python3 files into python3-stem so they can have correct dependencies
    - Add python3-stem to debian/control
    - Adjust debian/rules
  * Adjust python related build-depends so clean works.

  [ Ulises Vitulli ]
  * Deploy separated packages for 2 and 3 python series, thanks ScottKitterman
    (Closes: #712743).
  * Replaces and Breaks on older python-stem for safely transitioning.
  * Minor fixes at copyright machine readable format.

 -- Ulises Vitulli <dererk@debian.org>  Fri, 16 Aug 2013 12:11:14 +0200

python-stem (1.0.1-2) unstable; urgency=low

  * Fix FTBFS on unhandled py3.3 on Sid. Thanks JakubWilk (Closes: #707160).
  * Dropped unsed dep on pysocksipy. Thanks sramacher@ (Closes: #707563).
  * d/rules: Added python3.3 support.
  * d/compat: Fix debhelper compat version. Thanks lunar@.
  * d/control: Drop unused dep on python-support.
  * d/copyright: Add missing Copyright-Format header. Thanks lunar@.

 -- Ulises Vitulli <dererk@debian.org>  Sat, 11 May 2013 10:27:37 -0300

python-stem (1.0.1-1) unstable; urgency=low

  * Initial release (Closes: #697880) (Fixes upstream Trac#7906).

 -- Ulises Vitulli <dererk@debian.org>  Thu, 10 Jan 2013 16:06:33 -0300
